import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person.js';
// import Radium,{StyleRoot} from 'radium';

class App extends Component {
  state ={
    persons:[
      {id: "asd1", name:"Abhay", age:28},
      {id: "asd2", name:"Paras", age:29},
      {id: "asd3", name:"Lohia", age:50}
    ],
    otherState : "My unchanges state",
    showPerson: false,
  } 

  switchNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })

    const person = {
      ...this.state.persons[personIndex]
    }
    
    person.name = event.target.value;
    const persons = [...this.state.persons];
    
    persons[personIndex] = person;
    this.setState({
      persons:persons
    });
  }

  deletePersonHandler = (index) => {
    //const persons = this.state.persons;// adds a reference type constant;
    //const persons = this.state.persons.slice(); // creates a copy
    const persons = [...this.state.persons]; // using spread "..." operator
    persons.splice(index, 1);
    this.setState({persons:persons})
  }

  showPersonHandler = () => {
    const doesShow = this.state.showPerson;
    this.setState({
      showPerson:!doesShow,
    })
  }

  render() {
    const style = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      // ':hover':{
      //   backgroundColor: 'lightgreen',
      //   color: 'black',
      // }
    }
    let persons = null;
    if(this.state.showPerson) {
      persons = (
        <div>
          {
            this.state.persons.map((person, index) => {
              return <Person 
              click={() => this.deletePersonHandler(index)}
              name={person.name}
              age={person.age}
              key={person.id}
              change={(event) => this.switchNameHandler(event, person.id)}/>
            })
          }
          
          </div>
      )
      style.backgroundColor = 'red';
      // style[':hover'] = {
      //   backgroundColor: 'salmon',
      //   color: 'black'
      // }
    }
    const classes = [];
    if(this.state.persons.length <=2) {
      classes.push('red');
    }
    if(this.state.persons.length <=1) {
      classes.push('bold');
    }

    return (
      
      <div className="App">
        <h1>Hi, I am a react app</h1>
        <p className={classes.join(' ')}>This is working!!!</p>
        <button style={style} onClick={this.showPersonHandler}>Switch</button>
          {persons}
        
      </div>
      
    );
  }
}

export default App;
